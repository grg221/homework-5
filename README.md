# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.

## Question 1

Make all parentheses explicit in these λ- expressions:

a. ((λp.(pz))(λq.(w(λw.((((wq)z)p))))))
   
b.  (λp.((pq)(λp.(qp))))

## Question 2

In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

1. λs.s z λq.s q

    The second s would be bounded to the first, and the third s is free, q is bounded, and z is free.

2. (λs. s z) λq. w λw. w q z s

    The bounded variables are s, and w, w' is free, so is the second s, z is for the entire expression.

3. (λs.s) (λq.qs)

    s is bound by the first lambda, q is bound by the second, and the second s is free.

4. λz. (((λs.sq) (λq.qz)) λz. (z z))
    
      There are three sub-problems to the right of the first lamba. Since z is the main lambda, we say that all z's are bound. The 's' to the right of the second lambda is bound, and the 'q' to the right of the third lambda is bound.

## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.

1. (λz.z) (λq.q q) (λs.s a)

    replace z with q

		([(λq.qq) -> z] z)(λs.sa)) = (λq.qq)(λs.sa)

	  then replace q with sa

	    ([(λs.sa) -> q] qq) = (λs.sa)(λs'.s'a')

	  then

	  	([(λs'.s'a') -> s] sa) = (λs'.s'a') a

	  and

	  	([a -> s'] s' a') 

	  	= aa'

2. (λz.z) (λz.z z) (λz.z q)

    replace z with z'

		([(λz. z z) -> z] z) = (λz.zz)(λz.zq)

	   replace z with zq

	    ([λz.zq -> z] z z) = (λz.zq) (λz'.z'q')

	   	([λz'.z'q') -> z] z q) = (λz'.z'.q') q

	   	[q -> z'] z q' 

	   	= qq'

3. (λs.λq.s q q) (λa.a) b

    replace s with λa.a

		([(λa.a -> s) λq] s q q) b = (λq.(λa.a) qq) b

	   then replace with b

	   	([b -> q])(λa.a)qq = (λa.a)bb

	   then

	   ([bb -> a] a) 

	   = bb

4. (λs.λq.s q q) (λq.q) q

    replace s with (λq'.q')
		
		[λq.q -> s] λq.sqq = (λq.(λq.q)qq) q

		[q -> q](λq.q)qq = (λq'.q') q'q'

		[(q'q' -> q') q'] 

		= q''q'' 

5. ((λs.s s) (λq.q)) (λq.q)

    [(λq.q -> s) s s (λq.q)] = (λq.q)(λq.q)(λq.q)

	   [((λq.q) -> q) (λq.q)] = (λq'.q')(λq.q)(λq.q)

	   [λq.q -> q'] 

	   = (λq'.q')((λq.q))

	   [(λq.q) -> q') q']

	   = (λq.q)

## Question 4

1. Write the truth table for the or operator below.

        T T T
        T F T
        F F F
        F T T

2. The Church encoding for OR = (λp.λq.p p q)

Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. For the first derivation, show the long-hand solution (don't use T and F, use their definitions). For the other 3 you may use the symbols in place of the definitions. 

    TRUE TRUE:

	   (λp.λq.p p q)(λa.λb.a)(λa.λb.a)

	   ([(λa.λb.a) -> p] p p q)(λa.λb.a) = (λq.(λa.λb.a)(λa.λb.a)q)(λa.λb.a)

	   [(λa.λb.a) -> q](λq.(λa.λb.a)(λa.λb.a)q) = (λa.λb.a)(λa.λb.a)(λa.λb.a)

	   ([(λa.λb.a) -> a] λb.a)(λa.λb.a) = (λb.(λa.λb.a))(λa.λb.a) 

	   [(λa.λb.a -> b](λa.λb.a) = (λa.λb.a) = TRUE

	TRUE FALSE:

	   (λp.λq.p p q) TRUE FALSE

	   ([FALSE -> p] λq. p p q) TRUE = (λq.FALSE FALSE q) TRUE

	   [TRUE -> q] FALSE FALSE q = FALSE FALSE TRUE = TRUE

	FALSE TRUE:

	   (λp.λq.p p q) FALSE TRUE

	   ([FALSE -> p] (λp. p p q) T) = (λq. FALSE FALSE q) TRUE

	   ([TRUE -> q] FALSE FALSE q) = FALSE FALSE TRUE = TRUE

	FALSE FALSE:

	   (λp.λq.p p q) FALSE FALSE

	   ([FALSE -> p] λq. p p q) FALSE = (λq.FALSE FALSE q) FALSE

	   [FALSE -> q] FALSE FALSE q = FALSE FALSE FALSE = FALSE


## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.

    Negation in lamba calculus is defined as λx.x f t

    λt and λf are expressions for true and false. If TRUE (λa.λb.a) or FALSE (λa.λb.b) are added to the end, we negate our expression.

    This is similar to an IF statement because we can define IF as λx.λt.λf.x t f, which  returns t when true and f when false. 



